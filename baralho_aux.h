#ifndef BARALHO_AUX_H
#include "lista.h"
#define BARALHO_AUX_H

//Cartas que ja foram adicionadas, sobraram 30 para formarem o baralho auxiliar

#define QUANTIDADE_CARTAS_ADICIONADA_SEQUENCIA 74

lis_cabeca * BARAUX_Insere_Baralho_Auxiliar(mesa * paciencia, int * baralho, int * naipe, int * cartas_usadas);

#endif