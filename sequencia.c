/***************************************************************************
*  $MCI Módulo de implementação: Módulo Baralho
*
*  Arquivo gerado:              sequencia.C
*  Letras identificadoras:      SEQ
*
*  Nome da base de software:    Paciência Spider
*  Arquivo da base de software: D:\AUTOTEST\PROJETOS\SIMPLES.BSW (????)
*
*  Projeto: INF 1628 T2 Jogo Paciência Spider
*  Gestor:  Flavio Bevilacqua
*
*  Autores: Victor Sabino
*           Gabriel Medeiros
*           Henrique Rito
*
*  $HA Histórico de evolução:
*     Versão   Autor    Data     Observações
*       1.00   Victor   10/15/2015 Início do desenvolvimento
*
***************************************************************************/


#include "embaralha.h"
#include "sequencia.h"
#include <stdlib.h>
#include <math.h>





/***********************************************************************
*
*  $FC Função: Aux Insere Sequência
*
*  $ED Descrição da função
*  Insere uma sequência de cartas, deve ser chamada múltiplas vezes ,no caso do jogo paciência spider, 10 vezes.
*  
****************************************************************/

lis_cabeca * Aux_Insere_Sequencia(int pos, int * baralho, int * naipe, int * cartas_usadas){

	//num_cartas_por_sequencia é inicializado com 6, seguindo a regra do jogo Paciência Spider
	int i, num_cartas_por_sequencia = 6;
	//variavel que será usada para ligar com pAnt, na lista duplamente encadeada
	elemen * pAuxiliar_Ant = NULL;


	//aloca espaço para ser preenchido pela sequência de cartas
	lis_cabeca * pSequencia = (lis_cabeca *)malloc(sizeof(lis_cabeca));
	pSequencia->pOrigem = (elemen *)malloc(sizeof(elemen));
	//prepara pCorr para correr a sequência, inicializando-o com pOrigem
	pSequencia->pFim = NULL;
	pSequencia->pCorr = pSequencia->pOrigem;

	/* Seguindo as regras do jogo paciência spider, as primeiros 4 sequências tem 6 cartas, e as seguintes tem 5
	Usando essa lógica, a pos tem o valor de quantas cartas foram colocadas até agora, se for maior do que 24 então
	a sequencia é maior ou igual a quinta
	*/
	if (pos > 24)
		num_cartas_por_sequencia -= 1;
	//preenche as sequencias doos arrays baralho e naipe
	for (i = 0; i < num_cartas_por_sequencia; i++){

		pSequencia->pCorr->naipe = naipe[i+pos];
		//adiciona cartas viradas para baixo (cartas negativas) a lista encadeada
		pSequencia->pCorr->num_carta = -1 * baralho[i+pos];

		pSequencia->pCorr->pProx = (elemen *)malloc(sizeof(elemen));
		//inicializando variavel pAnt com a variavel auxiliar, que guarda o valor do anterior
		pSequencia->pCorr->pAnt = pAuxiliar_Ant;
		//avançando pAuxiliar_Ant para o atual
		pAuxiliar_Ant = pSequencia->pCorr;
		//avançando pCorr para pProx
		pSequencia->pCorr = pSequencia->pCorr->pProx;
	} /* for */
	//é iniciado pFim como a posição anterior e o pCorr como NULL
	pSequencia->pFim = pAuxiliar_Ant;
	//torna a ultima carta virada pra cima
	pSequencia->pFim->num_carta *= -1;
	pSequencia->pCorr = NULL;

	return pSequencia;
} /* Fim função: Aux Insere Sequência */

mesa * SEQ_Insere_Sequencia(mesa * paciencia, int * baralho, int * naipe, int * cartas_usadas){
	int i;
	for (i = 0; i <= 4; i++){
		paciencia->pListaCarta[i] = Aux_Insere_Sequencia(i * 6, baralho, naipe, cartas_usadas);
	}
	for (i = 4; i < 10; i++){
		paciencia->pListaCarta[i] = Aux_Insere_Sequencia(i * 5 + 25, baralho, naipe, cartas_usadas);
	}
	return paciencia;

} /* Fim função: SEQ Insere Sequência */


/* funcao de auxilio ao desenvolvimento
void print_sequencia(elemen * cartas){
	elemen * p = cartas;
	int i;
	for (i = 0 ; i < 5; i++){
		printf("%d\n", p->num_carta);
		p = p->pProx;
	}
}
*/
