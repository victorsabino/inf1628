/***************************************************************************
*  $MCI Módulo de implementação: Módulo de Comandos do jogo Pacienca e suas Regras
*
*  Arquivo gerado:              comandos.c
*
*  Nome da base de software:    Paciência Spider
*  Arquivo da base de software: D:\AUTOTEST\PROJETOS\SIMPLES.BSW (????)
*
*  Letras identificadoras:      PAC
*  Projeto: INF 1628 T2 Jogo Paciência Spider
*  Gestor:  Flavio Bevilacqua
*
*  Autores: Victor Sabino
*           Gabriel Medeiros
*           Henrique Rito
*
*  $HA Histórico de evolução:
*     Versão   Autor    Data     Observações
*       1.00   Victor   11/15/2015 Início do desenvolvimento
*
***************************************************************************/


#include "lista.h"


/***************************************************************************
*
*  Função: Verifica Movimento
*  ****/
int Verifica_Movimento(elemen * carta){
	/* se alguma dessas cartas estiver virada */
	if(carta->num_carta < 0){
		return 0;
	} /* if */
	//se a movimentacao nao for valida porque as cartas nao sao seguidas
	if(carta->num_carta != carta->pAnt->num_carta + 1){
		return 0;
	} /* if */
	//se os naipes das cartas sao diferentes
	if(carta->naipe != carta->pAnt->naipe)
		return 0
	} /* if */
}
