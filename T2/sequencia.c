/***************************************************************************
*  $MCI Módulo de implementação: Módulo Baralho
*
*  Arquivo gerado:              sequencia.C
*  Letras identificadoras:      SEQ
*
*  Nome da base de software:    Paciência Spider
*  Arquivo da base de software: D:\AUTOTEST\PROJETOS\SIMPLES.BSW (????)
*
*  Projeto: INF 1628 T2 Jogo Paciência Spider
*  Gestor:  Flavio Bevilacqua
*
*  Autores: Victor Sabino
*           Gabriel Medeiros
*           Henrique Rito
*
*  $HA Histórico de evolução:
*     Versão   Autor    Data     Observações
*       1.00   Victor   10/15/2015 Início do desenvolvimento
*
***************************************************************************/



#include "sequencia.h"
#include <stdlib.h>
#include <math.h>
#include <stdio.h>
int baralho[NUMERO_CARTAS];
int naipe[NUMERO_CARTAS];
static mesa * paciencia = NULL;



/***********************************************************************
*
*  $FC Função: Aux Insere Sequência
*
*  $ED Descrição da função
*  Insere uma sequência de cartas, deve ser chamada múltiplas vezes ,no caso do jogo paciência spider, 10 vezes.
*  
****************************************************************/

lis_cabeca * Aux_Insere_Sequencia(int pos, mesa * paciencia, int * baralho, int * naipe, int * cartas_usadas, MESA_tpCondRet * erro){
	
	//num_cartas_por_sequencia é inicializado com 6, seguindo a regra do jogo Paciência Spider
	int i, num_cartas_por_sequencia = 6;
	//variavel que será usada para ligar com pAnt, na lista duplamente encadeada
	elemen * pAuxiliar_Ant = NULL;


	//aloca espaço para ser preenchido pela sequência de cartas
	lis_cabeca * pSequencia = (lis_cabeca *)malloc(sizeof(lis_cabeca));
        if (!pSequencia) {
                * erro = MESA_CondRetFaltouMemoria;
		return NULL;
        }
	pSequencia->pOrigem = (elemen *)malloc(sizeof(elemen));
        if (!pSequencia->pOrigem) {
                * erro = MESA_CondRetFaltouMemoria;
		return NULL;
        }
	//prepara pCorr para correr a sequência, inicializando-o com pOrigem
	pSequencia->pFim = NULL;
	pSequencia->pCorr = pSequencia->pOrigem;

	/* Seguindo as regras do jogo paciência spider, as primeiros 4 sequências tem 6 cartas, e as seguintes tem 5
	Usando essa lógica, a pos tem o valor de quantas cartas foram colocadas até agora, se for maior do que 24 então
	a sequencia é maior ou igual a quinta
	*/
	if (pos > 24)
                * erro = SEQ_CondRetPosNaoFazParte;
		num_cartas_por_sequencia -= 1;
	//preenche as sequencias doos arrays baralho e naipe
	for (i = 0; i < num_cartas_por_sequencia; i++){
		
		pSequencia->pCorr->naipe = naipe[i+pos];
		//adiciona cartas viradas para baixo (cartas negativas) a lista encadeada
		pSequencia->pCorr->num_carta = -1 * baralho[i+pos];
		pSequencia->pCorr->pProx = (elemen *)malloc(sizeof(elemen));
		//inicializando variavel pAnt com a variavel auxiliar, que guarda o valor do anterior
		pSequencia->pCorr->pAnt = pAuxiliar_Ant;
		//avançando pAuxiliar_Ant para o atual
		pAuxiliar_Ant = pSequencia->pCorr;

		printf("%d, %d\n", pSequencia->pCorr->num_carta, i);
		//avançando pCorr para pProx
		pSequencia->pCorr = pSequencia->pCorr->pProx;
		
	} /* for */
	//é iniciado pFim como a posição anterior e o pCorr como NULL
	pSequencia->pFim = pAuxiliar_Ant;
	//torna a ultima carta virada pra cima
	pSequencia->pFim->num_carta *= -1;
	pSequencia->pCorr = NULL;

	return pSequencia;
} /* Fim função: Aux Insere Sequência */

mesa * SEQ_Insere_Sequencia(mesa * paciencia, int * baralho, int * naipe, int * cartas_usadas){
	int i;
	
	for (i = 0; i <= 4; i++){
		paciencia->pListaCarta[i] = Aux_Insere_Sequencia(i * 6 + 1, paciencia, baralho, naipe, cartas_usadas);
	}
	for (i = 4; i < 10; i++){
		paciencia->pListaCarta[i] = Aux_Insere_Sequencia(i * 5 + 1, paciencia, baralho, naipe, cartas_usadas);
	}
	return paciencia;

} /* Fim função: SEQ Insere Sequência */


/***************************************************************************
*
*  Função: Verifica Movimento
*  ****/

SEQ_tpCondRet Verifica_Movimento(elemen * carta){
	/* se alguma dessas cartas estiver virada */
	if(carta->num_carta < 0){
           if(!carta) {
                 return SEQ_CondRetCartaNull;
             }  
		return SEQ_CondRetCartaVirada;
	} /* if */
	//se a movimentacao nao for valida porque as cartas nao sao seguidas
	if(carta->num_carta != carta->pAnt->num_carta + 1){
		return SEQ_CondRetMovimentoInvalido;
	} /* if */
	//se os naipes das cartas sao diferentes
	if(carta->naipe != carta->pAnt->naipe){
		return SEQ_CondRetNaipesDiferentes;
	} /* if */
	return 1;
}

/***************************************************************************
*
*  Função: Vira Carta Cima
*  ****/


SEQ_tpCondRet Vira_Carta_Cima(elemen * carta){
	if(!carta){
		return SEQ_CondRetCartaNull;
	} /* if */
	if(carta->num_carta > 0){ 
		return SEQ_CondRetCartaVirada;	
	} /* if */
	carta->num_carta *= -1;
	return SEQ_CondRetCartaJaFoiVirada;	
}

SEQ_tpCondRet SEQ_Move_Cartas(mesa * paciencia, int fonte, int destino, int quantidade){
	elemen * carta_auxiliar = paciencia->pListaCarta[fonte]->pFim;
	int i;
	for(i = 0; i < quantidade; i++){
		if(Verifica_Movimento(carta_auxiliar) == 0)
			return SEQ_CondRetMovimentoInvalido;
		carta_auxiliar =  carta_auxiliar->pAnt;
	} /* for */
	//verifica se a carta fonte e igual a carta destino + 1, no caso como foi decrementado quantidade na fonte (carta_auxiliar), [e necessario incrementar quantidade para obtermos o valor 		real da comparacao e tambem verifica se os naipes sao iguais
	if (carta_auxiliar->num_carta == paciencia->pListaCarta[destino]->pCorr->num_carta + quantidade + 1 && paciencia->pListaCarta[fonte]->pCorr->naipe == paciencia->pListaCarta[fonte]->pCorr->naipe){
		for(i = 0; i < quantidade ; i++) {
			paciencia->pListaCarta[destino]->pFim->pProx = paciencia->pListaCarta[fonte]->pFim;
			paciencia->pListaCarta[fonte]->pFim = paciencia->pListaCarta[fonte]->pFim->pAnt;
			paciencia->pListaCarta[destino]->pFim = paciencia->pListaCarta[destino]->pFim->pProx;
		} /* for */
		Vira_Carta_Cima(paciencia->pListaCarta[fonte]->pFim);
		//return ok
		return SEQ_CondRetOK;	
	
	} /* if */
	return SEQ_CondRetErroAoMoverCarta;
}

