/***************************************************************************
*  $MCI Módulo de implementação: Módulo Mesa
*
*  Arquivo gerado:              mesa.c
*  Letras identificadoras:      mesa
*
*  Nome da base de software:    Paciência Spider
*  Arquivo da base de software: D:\AUTOTEST\PROJETOS\SIMPLES.BSW (????)
*
*  Projeto: INF 1628 T2 Jogo Paciência Spider
*  Gestor:  Flavio Bevilacqua
*
*  Autores: Victor Sabino
*           Gabriel Medeiros
*           Henrique Rito
*
*  $HA Histórico de evolução:
*     Versão   Autor    Data     Observações
*       1.00   Victor   10/15/2015 Início do desenvolvimento
*
***************************************************************************/

#include "stdlib.h"
#include "mesa.h"
#include "sequencia.h"
#include "baralho_aux.h"

#include <stdlib.h>


/***********************************************************************
*
*  $FC Função: BAR Inicializa Mesa
*
*  $ED Descrição da função
*  Aloca espaço pra variável mesa, e adiciona dificuldade escolhida para o jogo
*
****************************************************************/

int baralho[NUMERO_CARTAS];
int naipe[NUMERO_CARTAS];
int cartas_usadas[NUMERO_CARTAS];
static mesa * paciencia = NULL;

void MESA_Embaralha_Cartas();

mesa * MESA_Cria_Mesa(int dificuldade, MESA_tpCondRet * erro){
		int i;
	//baralho[NUMERO_CARTAS]: variável auxilar que guarda as cartas

	/* carta_usada[i] : verifica se a iésima carta já foi acessada
	ex: if(carta_usada[1] == -1) então a carta ainda não foi inserida */


	if(dificuldade <= 0 || dificuldade >= 5){
                * erro = MESA_CondRetErroDificuldadeInvalida;
		return NULL;
	}

	paciencia = (mesa *) malloc(sizeof(mesa));
	if (!paciencia) {
                * erro = MESA_CondRetFaltouMemoria;
		return NULL;
       }
	//inicia dificuldade
	paciencia->dificuldade = dificuldade;
	paciencia->pListaCarta = (lis_cabeca **) malloc(10 * sizeof(lis_cabeca));
	if (!paciencia->pListaCarta) {
                * erro = MESA_CondRetFaltouMemoria;
		return NULL;
        }
	for (i = 0; i < 10; i++){
		paciencia->pListaCarta[i] = NULL;
	} /* for */

	paciencia->pBaralho = (lis_cabeca *) malloc(sizeof(lis_cabeca));
	if (!paciencia->pBaralho) {
                * erro = MESA_CondRetFaltouMemoria;
		return NULL;
       }
	MESA_Inicializa_Mesa();
		


	return paciencia;
	
}

MESA_tpCondRet MESA_Inicializa_Mesa( MESA_tpCondRet * erro){


	int i, returnTmp;

	if (!paciencia) {
                * erro = MESA_CondRetFaltouMemoria;
		return MESA_MesaVazia;
        }
	if (!paciencia->pListaCarta) {
                * erro = MESA_CondRetFaltouMemoria;
		return MESA_SequenciaVazio;
        }
	if (!paciencia->pBaralho) {
                * erro = MESA_CondRetFaltouMemoria;
		return MESA_BaralhoAuxiliarVazio;
        }

	//chama embaralha carta
	MESA_Embaralha_Cartas();
	
	paciencia = SEQ_Insere_Sequencia(paciencia, baralho, naipe, cartas_usadas);
	
	paciencia->pBaralho = BARAUX_Insere_Baralho_Auxiliar(paciencia, baralho, naipe, cartas_usadas);
	
	return MESA_CondRetOK;
} /* Fim função: BAR Inicializa Mesa */

/***************************************************************************
*
*  Função: Embaralha Cartas
*  ****/

int Inicia_Naipes(){
	int i;
	for (i = 0; i < NUMERO_CARTAS; i++){
		naipe[i] = -1;
		cartas_usadas[i] = -1;
	}  /* for */
	return 0;
}

void MESA_Embaralha_Cartas(){
	int i, divisor, carta_tmp;
	int count = 0;
	//iniciando naipe com -1 e cartas_usadas com -1
	Inicia_Naipes();

	//embaralhando cartas
	for (i = 0; i < NUMERO_CARTAS; i++){
		//pega uma carta aleatoria entre 1 e NUMERO_CARTAS (102)		
		carta_tmp = rand() % NUMERO_CARTAS + 1;
		//enquanto não achar uma carta não usada, vai avançar entre 1 e NUMERO_CARTAS
		while (cartas_usadas[carta_tmp] != -1){
			carta_tmp += 1;
			carta_tmp %= NUMERO_CARTAS;
		} /* while */

		//marca carta_tmp como carta usada
		cartas_usadas[carta_tmp] = 1;
		//se carta_tmp > 52 então é o segundo baralho e será divido por 2, senão é o primeiro baralho e não precisa ser divido
		divisor = carta_tmp >= 52 ? 52 : carta_tmp;
		//acha os valores da carta pegando o resto de 13 (número de cartas de um baralho por naipe)
		baralho[i] = carta_tmp % 13 + 1;
		//acha o naipe
		naipe[i] = (carta_tmp / 13) % 4 + 1;
	} /* for */

} /* Fim função: BAR Embaralha Cartas */



