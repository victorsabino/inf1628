#ifndef MESA_H
#define MESA_H
#include "lista.h"
typedef enum {
         MESA_CondRetOK = 0 ,
               /* Executou correto */

         MESA_CondRetErroEstrutura = 1 ,
               /* Estrutura da árvore está errada */

        MESA_CondRetFaltouMemoria = 3 ,
               /* Faltou memória ao alocar dados */
	MESA_CondRetErroDificuldadeInvalida = 4
                /* Valor da dificuldade diferente do esperado */
        MESA_CondRetMesaVazia = 5 ,
        	/* Mesa não inicializada ou Inicializada incorretamente */
        MESA_CondRetSequenciaVazio = 6 ,
        	/* Mesa não inicializada ou Inicializada incorretamente */
        MESA_CondRetBaralhoAuxiliarVazio = 7 
        	/* Mesa não inicializada ou Inicializada incorretamente */
   } MESA_tpCondRet ;


MESA_tpCondRet MESA_Inicializa_Mesa( MESA_tpCondRet * erro);
mesa * MESA_Cria_Mesa( int dificuldade, MESA_tpCondRet * erro );


#undef MESA_H
#endif
