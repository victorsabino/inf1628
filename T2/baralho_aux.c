/***************************************************************************
*  $MCI Módulo de implementação: Módulo de Baralho Auxiliar
*
*  Arquivo gerado:              baralho_aux.c
*
*  Nome da base de software:    Paciência Spider
*  Arquivo da base de software: D:\AUTOTEST\PROJETOS\SIMPLES.BSW (????)
*
*  Projeto: INF 1628 T2 Jogo Paciência Spider
*  Gestor:  Flavio Bevilacqua
*
*  Autores: Victor Sabino
*           Gabriel Medeiros
*           Henrique Rito
*
*  $HA Histórico de evolução:
*     Versão   Autor    Data     Observações
*       1.00   Victor   11/15/2015 Início do desenvolvimento
*
***************************************************************************/
#include "baralho_aux.h"
#include <stdlib.h>
/***********************************************************************
*
*  $FC Função: Insere Sequência
*
*  $ED Descrição da função
*  Insere as 30 cartas restantes no baralho auxiliar
*
****************************************************************/

lis_cabeca * BARAUX_Insere_Baralho_Auxiliar(mesa * paciencia, int * baralho, int * naipe, int * cartas_usadas){
	//variavel que será usada para ligar com pAnt, na lista duplamente encadeada
	elemen * pAuxiliar_Ant = NULL;
	int i;

	//aloca espaço para ser preenchido pela sequência de cartas
	lis_cabeca * pSequencia = (lis_cabeca *)malloc(sizeof(lis_cabeca));
        if (!pSequencia) {
                * erro = BAR_CondRetFaltouMemoria;
		return NULL;
        }
	pSequencia->pOrigem = (elemen *)malloc(sizeof(elemen));
        if (!pSequencia->pOrigem) {
                * erro = BAR_CondRetFaltouMemoria;
		return NULL;
        }
	//prepara pCorr para correr a sequência, inicializando-o com pOrigem
	pSequencia->pFim = NULL;
	pSequencia->pCorr = pSequencia->pOrigem;

	//preenche as sequencias doos arrays baralho e naipes
	// enquanto i for menor que 30 (cartas a serem adicionadas no baralho extra
	for (i = 0; i < 30; i++){

		pSequencia->pCorr->naipe = naipe[i+QUANTIDADE_CARTAS_ADICIONADA_SEQUENCIA];
		pSequencia->pCorr->num_carta = baralho[i+QUANTIDADE_CARTAS_ADICIONADA_SEQUENCIA];

		pSequencia->pCorr->pProx = (elemen *)malloc(sizeof(elemen));
		//inicializando variavel pAnt com a variavel auxiliar, que guarda o valor do anterior
		pSequencia->pCorr->pAnt = pAuxiliar_Ant;
		//avançando pAuxiliar_Ant para o atual
		pAuxiliar_Ant = pSequencia->pCorr;
		//avançando pCorr para pProx
		pSequencia->pCorr = pSequencia->pCorr->pProx;
	} /* for */
	//é iniciado pFim como a posição anterior e o pCorr como NULL
	pSequencia->pFim = pAuxiliar_Ant;
	pSequencia->pCorr = NULL;

	return pSequencia;
} /* Fim função: BAR Inicializa Mesa */
