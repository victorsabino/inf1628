#ifndef LISTA_H

#define LISTA_H

#define NUMERO_CARTAS 104
#define NUMERO_POR_NAIPE 13

typedef struct Mesa mesa;
typedef struct Lis_cabeca lis_cabeca;
typedef struct Elemen elemen;
typedef struct Carta carta;
struct Mesa{
	int dificuldade;
	struct Lis_cabeca ** pListaCarta;
	struct Lis_cabeca * pBaralho;
};

struct Lis_cabeca{
	struct Elemen * pOrigem;
	struct Elemen * pFim;
	struct Elemen * pCorr;
	int quantidade_cartas;
};

struct Elemen{
	struct Elemen * pAnt;
	struct Elemen * pProx;
	int num_carta;
	int naipe;
};



#endif