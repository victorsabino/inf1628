#ifndef SEQUENCIA_H
#define SEQUENCIA_H

#include "lista.h"
typedef enum {

         SEQ_CondRetOK = 0 ,
               /* Executou correto */
         SEQ_CondRetPosNaoFazParte = 1 ,
                /*  */
         SEQ_CondRetCartaVirada = 2 ,
                /*  */
         SEQ_CondRetMovimentoInvalido = 3 ,
                /*  */
         SEQ_CondRetNaipesDiferentes = 4 ,
                /*  */
         SEQ_CondRetCartaNull = 5 ,
                /*  */
         SEQ_CondRetCartaJaFoiVirada = 6 
                /*  */
         SEQ_CondRetErroAoMoverCarta = 7
                /*  */

   } SEQ_tpCondRet ;

lis_cabeca * Aux_Insere_Sequencia(int pos, mesa * paciencia, int * baralho, int * naipe, int * cartas_usadas, MESA_tpCondRet * erro);
mesa * SEQ_Insere_Sequencia(mesa * paciencia, int * baralho, int * naipe, int * cartas_usadas);
SEQ_tpCondRet Verifica_Movimento(elemen * carta);
SEQ_tpCondRet Vira_Carta_Cima(elemen * carta);
SEQ_tpCondRet SEQ_Move_Cartas(mesa * paciencia, int fonte, int destino, int quantidade)/

#endif
