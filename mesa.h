#ifndef MESA_H
#define MESA_H
#include "lista.h"
typedef enum {

         MESA_CondRetOK = 0 ,
               /* Executou correto */

         MESA_CondRetErroEstrutura = 1 ,
               /* Estrutura da árvore está errada */

        MESA_CondRetFaltouMemoria = 3 ,
               /* Faltou memória ao alocar dados */
        MESA_CondRetDificuldadeInvalida = 4 ,
        	 /* Dificuldade não inicializada corretamente */
        MESA_MesaVazia = 5 ,
        	/* Mesa não inicializada ou Inicializada incorretamente */
        MESA_SequenciaVazio = 6 ,
        	/* Mesa não inicializada ou Inicializada incorretamente */
        MESA_BaralhoAuxiliarVazio = 7 ,
        	/* Mesa não inicializada ou Inicializada incorretamente */
   } MESA_tpCondRet ;

void MESA_Embaralha_Cartas( void );
MESA_tpCondRet MESA_Inicializa_Mesa(int dificuldade);
MESA_tpCondRet MESA_Cria_Mesa( void );


#undef MESA_H
#endif